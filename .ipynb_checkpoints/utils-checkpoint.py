import numpy as np


def sigmoid(z, tol=0.000001):
    """
    Get the sigmoid function for a given value

    :param z: value
    :param tol: threshold to account of edge cases of ~1 and ~0
    :return: value of the sigmoid function for <z>
    """
    sig = 1 / (1 + np.exp(-z))

    # edge case fix
    sig = np.maximum(sig, tol)
    sig = np.minimum(sig, 1 - tol)

    return sig


def sliding_window(initial_value, end_value, step_size):
    """
    Generates a sliding window from <initial_value> to <end_value> of a given step size, with
    the last one being smaller if so is necessary.

    :param initial_value: starting value (included)
    :param end_value: end value (included)
    :param step_size: value of each step size (except the last one)
    :return: slide_start, slide_end
    """
    prev = initial_value
    for i in range(step_size, end_value, step_size):
        yield prev, i
        prev = i

    # end slice
    yield prev, end_value


def shuffle_multiple(x1, x2):
    """

    :param x1:
    :param x2:
    :return:
    """
    ind = np.arange(x1.shape[0])
    np.random.shuffle(ind)
    return x1[ind], x2[ind]
