import numpy as np
import pandas as pd
import utils as ut


class LogisticRegression:
    """ My implementation of the Logistic Regression algorithm """

    def __init__(self, max_iter=100, learning_rate=0.01, reg_lambda=0.1, batch_size=100, warm_start=False):
        # Initialize parameters
        self._max_iter = max_iter
        self._learning_rate = learning_rate
        self._lambda = reg_lambda
        self._batch_size = batch_size
        self._warm_start = warm_start

        self._weights = np.array([])
        self._bias = 0
        self._costs = []

    def fit(self, x, y):
        """ Fits the model to the data provided

        :param x: matrix / dataframe with the features for each instance
        :param y: true labels for the data instances
        """
        # convert to numpy if coming from pandas
        if isinstance(x, pd.DataFrame):
            x = x.to_numpy()
        if isinstance(y, pd.DataFrame) or isinstance(y, pd.Series):
            y = y.to_numpy().reshape(-1, 1)

        # initialize weights & bias if no warm_start
        m = x.shape[0]
        if not self._warm_start:
            self._weights = np.random.rand(1, x.shape[1])
            self._bias = 0.5

        # iterate _max_iter number of times
        for i in range(self._max_iter):

            # shuffle the data set
            x, y = ut.shuffle_multiple(x, y)

            # create get the batches
            for step_init, step_end in ut.sliding_window(0, m, self._batch_size):

                # calculate the activation and cost values
                activation = ut.sigmoid(np.dot(x[step_init:step_end], self._weights.T) + self._bias)
                self._costs.append(self.cost(activation, y[step_init:step_end]))

                # calculate the gradients
                dw = ((1 / m) * np.dot((activation - y[step_init:step_end]).T, x[step_init:step_end])) + \
                     ((self._lambda / m) * self._weights)
                db = (1/m) * np.sum(activation - y[step_init:step_end])

                # update the weights and bias
                self._weights -= self._learning_rate * dw
                self._bias -= self._learning_rate * db

            activations = ut.sigmoid(np.dot(x, self._weights.T) + self._bias)
            self._costs.append(self.cost(activations, y))

    def predict(self, x, threshold=0.5):
        """ Given a dataset with new instances, make a prediction for the expected label
        
        :param x: Data to predict
        :param threshold: probability threshold by which to classify an instance as positive
        :return: predicted class
        """
        # convert to numpy if coming from pandas
        if isinstance(x, pd.DataFrame):
            x = x.to_numpy()

        activation = ut.sigmoid(np.dot(x, self._weights.T) + self._bias)
        return [1 if a > threshold else 0 for a in activation]

    def cost(self, activations, y):
        """ Cost function

        :param activations:
        :param y:
        :return:
        """
        m = activations.shape[0]
        cost = np.squeeze(-(1 / m) * np.sum(y * np.log(activations) + (1 - y) * np.log(1 - activations)))
        reg = (self._lambda / 2 * m) * np.sum(np.power(self._weights, 2))

        return cost + reg

